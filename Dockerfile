FROM openjdk:8-jdk-alpine
EXPOSE 8080
ADD ./target/reservation-service-1.0-SNAPSHOT.jar reservation-service-1.0-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/reservation-service-1.0-SNAPSHOT.jar"]
